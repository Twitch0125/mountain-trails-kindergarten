/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    fontFamily: {
      sans: ['Poppins', 'sans-serif'],
      serif: ['Zilla Slab', 'sans-serif']
    }
  },
  variants: {},
  plugins: []
}
